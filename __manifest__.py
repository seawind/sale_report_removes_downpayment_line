# -*- coding: utf-8 -*-
{
    'name': "sale_report_removes_downpayment_line",

    'summary': """
        Removes the downpayment line in the sales order report""",
    
    'category': 'Generic Modules/Sale',
    'author':   "Magboard LLC, "                
                "Odoo Community Association (OCA)", 
    'license': 'AGPL-3', 
    'website': "",
    'category': 'Sale',
    'version': '11.0.1.0.0',
    'depends': ['base','sale'],
    'data': [
	     'report/sale_report_removes_downpayment_line.xml',
            ],
    'installable': True   
}
